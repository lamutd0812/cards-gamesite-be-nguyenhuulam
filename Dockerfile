FROM node:14-alpine

WORKDIR /cards-game-nguyenhuulam

COPY package*.json ./

RUN npm i

COPY . .

EXPOSE 8080

CMD [ "npm", "run", "start" ]
