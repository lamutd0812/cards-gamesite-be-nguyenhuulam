const express = require('express');
const cardsController = require('../controllers/cards.controller');

const router = express.Router();

router.get('/see-cards/:email', cardsController.seeCards);

router.put('/update-cards', cardsController.updateCards);

module.exports = router;