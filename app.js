const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
require('dotenv').config();

const cardsRoute = require('./routes/cards.route');

const app = express();

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use('/cards', cardsRoute);

const PORT = process.env.NODE_APP_PORT || 8080;
app.listen(PORT, () => {
  console.log(`server running at port ${PORT}!`);
});