const axios = require('axios');

const marketApiUrl = process.env.MARKETPLACE_API_URL;

const instance = axios.create({
  baseURL: marketApiUrl,
});

module.exports = instance;