const axios = require('../utils/axios');
const {
  SEE_CARDS_SERVER_ERROR,
  UPDATE_CARDS_SERVER_ERROR,
} = require('../utils/constants.json');

exports.seeCards = async (req, res) => {
  try {
    const email = req.params.email;
    axios.get(`cards/see-cards/${email}`)
      .then(resp => {
        res.status(200).json(resp.data);
      })
      .catch(err => {
        const errData = err.response.data;
        res.status(errData.statusCode).json(errData);
      });
  } catch (err) {
    console.log(SEE_CARDS_SERVER_ERROR, err);
    res.status(500).json(SEE_CARDS_SERVER_ERROR);
  }
};

exports.updateCards = async (req, res) => {
  try {
    axios.put(`cards/update-cards`, req.body)
      .then(resp => {
        res.status(200).json(resp.data);
      })
      .catch(err => {
        const errData = err.response.data;
        res.status(errData.statusCode).json(errData);
      });
  } catch (err) {
    console.log(UPDATE_CARDS_SERVER_ERROR, err);
    res.status(500).json(UPDATE_CARDS_SERVER_ERROR);
  }
};